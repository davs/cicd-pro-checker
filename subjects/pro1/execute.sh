# Execute the command `$2` within folder `$1` if it exists
# List of labs are passed as space-separated list of folders

#!/bin/bash

CompileErrorHandler () {
    (( errcount++ ))
}

trap CompileErrorHandler ERR

if [ -d "$1" ]; then
  echo "####### Running command '$2' within '$1' ..."
  cd "$1"
  eval $2
  cd ..
else
    echo "####### Skipping '$1' ..."
fi

exit $errcount
