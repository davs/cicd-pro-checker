# Doxygen the project
# List of labs are passed as space-separated list of folders

#!/bin/bash

CompileErrorHandler () {
    (( errcount++ ))
}

trap CompileErrorHandler ERR

for lab in "$@" ; do
if [ -d "$lab" ]; then
  echo "####### Check Doxygen ${lab} ..."
  cd $lab && rm -rf dist && mkdir dist
  cat /util/Doxyfile > Doxyfile
  doxygen Doxyfile
  cd ..
else
    echo "####### Skipping ${lab} ..."
fi
done

exit $errcount
