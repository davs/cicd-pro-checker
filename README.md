# CI/CD Process for subject "Programing"

## Main Checker

Задача данного репозитория - формирование докер контейнера, который будет выполнять сборку лаборааторных работ



## Resources (C, subjects/pro1)

Данный репозиторий предназначен для хранения ресурсов автоматической сборки проектов по дисциплине "Программирование ч.1"

- `.gitlab-ci.yml`: сборочный файл гитлаба
- `.globlintrc.yml` - файл конфигурации структуры директории для дисциплины проекта [globlint](https://gitlab.com/malokhvii-eduard/globlint)

Этапы:

- проверка структуры директории, используя проект `globlint`
- компиляция, используя `clang`;
- провера на стилистику, используя `clang-format`;
- проверка качества кода статическим анализатором `cppcheck` (с включенным модулем cert);
- проверка на утечки памяти при помощи утилиты `valgrind`;
- (8+) проверка документированности кода, запуская `doxygen` с нужными параметрами;
   - [- FIXME: reuse: Doxyfile and get rid of doxygen.sh; remove me and use execute.sh: /util/execute.sh labXX mkdir -p dist && {wget Doxyfile} doxygen Doxyfile -]
- [- TODO: -] clang-tidy
- [- TODO: -] shell-checker

### Другое

Чтобы обезопасить систему от преднамеренной порчи clang-tidy/format файлов (чтобы "обмануть систему"), при старте образа с этими проверками происходит стягивание соответствующего файла из sample_project

## Reources (C/C++, subjects/pro2)

Данный репозиторий предназначен для хранения ресурсов автоматической сборки проектов по дисциплине "Программирование ч.2" (для Кибербезопасников - Основы безопасного прграммирования), а также РГЗ по данной дисциплине

- `.gitlab-ci.yml`: сборочный файл гитлаба
- `.globlintrc.yml` - файл конфигурации структуры директории для дисциплины проекта [globlint](https://gitlab.com/malokhvii-eduard/globlint)

По мимо этапов предшественника, добавляются следующище этапы:

- (17+) [- TODO: -] проверку на покрытость модульными тестами при помощи утилиты `lcov` , компиляция и запуск тестового модуля
- [- TODO: -] copy-paste detector

# Проекты для само-проверки

Все тестовые проекты, выполненные студентами, находятся тут: https://gitlab.com/javanix/pro1-test

[- их ci/cd файл (+дир-чекер конфигуратор) должен быть версионирован -]


# Other

- [- TODO: -] Diff-reviver introuction
- [- TODO: -] (TAO) Assessment system
   - ideally find / write oauth2 module
- [- TODO: -] bookshelf / resource manager
- [- TODO: -] external database like firebase

